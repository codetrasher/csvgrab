package csvgrab

import (
	"encoding/csv"
	"os"
)

type SingleSearchHelper struct {
	file *os.File
}

type MultiSearchHelper struct {
	files []*os.File
}

// SearchOne opens a file specified by filename parameter. An *os.File
// instance is assigned into the SingleSearchHelper struct pointer which
// is then returned.
func SearchOne(filename string) *SingleSearchHelper {

	var err error
	sh := &SingleSearchHelper{}
	if sh.file, err = os.Open(filename); err != nil {
		return nil
	}
	return sh
}

// SearchMany opens a number of files, inserts their os.File pointer to an array
// and returns the MultiSearchHelper struct.
func SearchMany(files ...string) *MultiSearchHelper {

	if len(files) < 2 {
		return nil
	}

	var err error
	mh := &MultiSearchHelper{files: make([]*os.File, len(files))}
	for i, file := range files {
		if mh.files[i], err = os.Open(file); err != nil {
			return nil
		}
	}
	return mh
}

// All reads all records from a single file.
func (sh *SingleSearchHelper) All() [][]string {
	defer sh.file.Close()
	return readAll(sh.file)
}

// All reads all values from all opened files.
func (mh *MultiSearchHelper) All() map[string][][]string {
	defer mh.closeAll()
	res := make(map[string][][]string)

	for _, fi := range mh.files {
		res[fi.Name()] = readAll(fi)
	}

	return res

}

func (mh *MultiSearchHelper) closeAll() {
	for _, f := range mh.files {
		f.Close()
	}
}

func readAll(f *os.File) [][]string {
	r := csv.NewReader(f)
	csvResult, _ := r.ReadAll()
	result := make([][]string, len(csvResult))
	result = csvResult

	// Omit the first row, which are the field names
	removeFields(&result)
	return result
}

func removeFields(r *[][]string) {
	*r = append((*r)[:0], (*r)[1:]...)
}
